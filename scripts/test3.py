import numpy as np
import matplotlib.pyplot as plt

from gauntlet_lib.potential import attr, repulse
from gauntlet_lib.features import rcirc, process, multipleRansac


data = process(np.load('scan8.npy'))
r = .11
lines, other = multipleRansac(data, 512, .005, .08, 5)
center, other = rcirc(other, r, 512, .005, 6)

thetas = np.linspace(0, 2 * np.pi)


plt.plot(data[0], data[1], '.')
plt.plot(other[0], other[1], '.')


p = np.array([0, 0])

repulseTotD = np.zeros(2)
for line in lines:
    kLine = line[:, 1] - line[:, 0]
    slope = kLine[1] / kLine[0]
    dr = repulse(slope, line[1, 0] - slope *
                 line[0, 0], line[0, 0], line[0, 1], p[0], p[0])
    repulseTotD += dr
vec = -attr(center[0], center[1], p[0], p[1]) - repulseTotD

X, Y = np.meshgrid(np.linspace(-2, 2, 100), np.linspace(-2, 2, 100))

repulseTot = np.zeros((2, 100, 100))
for line in lines:
    kLine = line[:, 1] - line[:, 0]
    slope = kLine[1] / kLine[0]
    dr = repulse(slope, line[1, 0] - slope *
                 line[0, 0], line[0, 0], line[0, 1], X, Y)
    repulseTot += dr

V = attr(center[0], center[1], X, Y) + repulseTot / 16

plt.streamplot(X, Y, V[0], V[1])
plt.quiver(p[0], p[1], vec[0], vec[1])

if center.size != 0:
    z = [r * np.cos(theta) for theta in thetas] + center[0]
    y = [r * np.sin(theta) for theta in thetas] + center[1]
    plt.plot(z, y, 'r', linewidth=4)

for line in lines:
    plt.plot(line[0], line[1], 'b', linewidth=4)


ax = plt.gca()
ax.set_aspect('equal')

plt.legend(('obstacle', 'lidar points', 'bucket'))
ax.set_title('LIDAR Map')
ax.set_xlabel('meters')
ax.set_ylabel('meters')
plt.savefig('test.png')
plt.show()
