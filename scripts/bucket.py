#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import PointStamped
from features import process, multipleRansac, findCircle
import numpy as np


class Bucket:
    def __init__(self):
        rospy.init_node('bucket')
        self.pubBucket = rospy.Publisher('bucket', PointStamped, queue_size=1)
        self.point = PointStamped()
        self.time = rospy.Time()
        self.sub = rospy.Subscriber('stable_scan', LaserScan, self.callback)
        self.point.header.frame_id = 'base_link'
        self.pts = np.reshape([], (2, 0))
        self.spin()

    def callback(self, data):
        self.pts = process(
            np.stack((np.linspace(0, 2 * np.pi, 361), data.ranges)))

    def findBucket(self):
        if self.pts.shape[1] != 0:
            center = rcirc(data, r, 512, .002, 8)
            self.point.header.seq += 1
            self.point.header.stamp = self.time.now()

            if center.size != 0:
                self.point.point.x = bucket[0]
                self.point.point.y = bucket[1]
                self.pubBucket.publish(self.point)

    def spin(self):
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            self.findBucket()
            rospy.spin()
            rate.sleep()


if __name__ == '__main__':
    bucket = Bucket()
