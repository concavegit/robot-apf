import numpy as np
import matplotlib.pyplot as plt

from gauntlet_lib.features import rcirc, process, multipleRansac


data = process(np.load('scan8.npy'))
r = .11
lines, other = multipleRansac(data, 512, .004, .08, 5)
center, other = rcirc(other, r, 512, .005, 6)
print(lines.shape)

thetas = np.linspace(0, 2 * np.pi)

plt.plot(data[0], data[1], '.')

if center.size != 0:
    z = [r * np.cos(theta) for theta in thetas] + center[0]
    y = [r * np.sin(theta) for theta in thetas] + center[1]
    plt.plot(z, y, 'r', linewidth=4)

for line in lines:
    plt.plot(line[0], line[1], 'b', linewidth=4)

ax = plt.gca()
ax.set_aspect('equal')
plt.legend(('bucket', 'lidar points', 'obstacle'))
ax.set_title('LIDAR Map')
ax.set_xlabel('meters')
ax.set_ylabel('meters')
plt.savefig('test2.png')

plt.show()
